.. include:: ../Includes.txt

.. _introduction:

============
Introduction
============

With this extension you can run selected extensions and files in an
error level of your choosing, and only see the errors, warnings and
notifications that you choose.

.. _what-it-does:

What does it do?
================

.. _Strict-mode-Most-verbose-error-reporting:

Strict mode: Most verbose error reporting
"""""""""""""""""""""""""""""""""""""""""

When PHP runs your code it generates information about the quality of
your code, e.g. notices, warnings and suggestions, to make your code
more interoperable and forward compatible. TYPO3 turns these messages
off in init.phpby calling :code:`error\_reporting` :code:`(`
:code:`E\_ALL` :code:`^` :code:`E\_NOTICE` :code:`);.` TYPO3 Core
would generate a lot of messages if `E\_NOTICES, E\_WARNINGS and
E\_STRICT <http://php.net/manual/en/errorfunc.constants.php>`_ were
turned on.

PHPs most verbose reporting level is: :code:`error\_reporting`
:code:`(` :code:`E\_ALL \|` :code:`E\_STRICT` :code:`);` . Running
code with that setting we call  *strict mode* .


.. _Features:

Features
""""""""

- Obeys devIPmasksetting, so only  *you* will see the messages – never
  your users.

- During installation pedantic will suggests you to install two
  companion extensions, and pedantic can redirect messages to any of
  these based on settings in the Extension Manager. The two suggested
  extension are:
  
  - EXT: `cc\_debug
    <http://typo3.org/extensions/repository/view/cc_debug/current/>`_ .
    Provides debug information in a pop-up window.
  
  - EXT: `devlog
    <http://typo3.org/extensions/repository/view/devlog/current/>`_ .
    Provides a back-end module for logging.

Easily turns strict mode on and off. Sometimes you just want to “code
away” without being buggered ;-)


.. _Benefits:

Benefits
""""""""

- Improve your code quality, since the messages suggest good coding
  practices.

- Find possible errors early on, e.g. a variable that is used but not
  declared because of a simple typo.

- Prepare for TYPO3 v5, which runs in strict mode, by developing all
  your extension to run in strict mode.


.. _Why-is-the-extension-called-pedantic:

Why is the extension called “pedantic”?
"""""""""""""""""""""""""""""""""""""""

The name “pedantic” is a tribute to GCC which has a  ``-pedantic``
flag that will make GCC complain about sloppy programming.

The extension icon, |img-3| , alludes to the student, who always has a
detail he want to get straight before moving on. When starting to run
in strict mode it is easy to feel that PHP is such a student.


.. _Screenshots:

Screenshots
^^^^^^^^^^^

|img-4|

This first screen shot shows the configuration screen (which is in the
Extension Manager).

**Extension key list** : List the extension keys you wish *pedantic*
to report messages from. In the screen shot three extensions are
given.

**Pattern for matching error filename(s)** : Write a regular
expression that matches the files you wish to be notified of
errors/warnings in. In the example the reg. exp. matches the file
``class.t3lib\_div.php`` and thus errors and warnings from the class
``t3lib\_div`` will be reported.

In the three drop-down boxes you select which messages you would like
and how they should be reported.
