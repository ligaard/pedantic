<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2008 Kasper Ligaard (kli@systime.dk)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for checking the PHPUnit 3.3.x
 * @author	Kasper Ligaard <ligaard@daimi.au.dk>
 */

require_once (PATH_t3lib.'class.t3lib_tcemain.php');

class t3lib_div_testcase extends tx_phpunit_testcase {
	private $prevErrorHandler;
	private $prevLevelSettings = array();

	protected function setUp() {
		// Save current
		$this->prevLevelSettings['reportNotice'] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportNotice'];
		$this->prevLevelSettings['reportWarning'] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportWarning'];
		$this->prevLevelSettings['reportStrict'] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportStrict'];

		// 'normalise' (each test should set this appropriately for it's own use).
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportNotice'] = false;
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportWarning'] = false;
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportStrict'] = false;

		// Reset counters to zero.
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']['notices'] = 0;
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']['warnings'] = 0;
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']['strict'] = 0;

		$this->prevErrorHandler = set_error_handler('t3lib_div_testcase::pedantic_errorHandlerCounter');
	}

	protected function tearDown() {
		set_error_handler($this->prevErrorHandler);
		$this->prevErrorHandler = null;

		// Restore level settings
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportNotice'] = $this->prevLevelSettings['reportNotice'];
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportWarning'] = $this->prevLevelSettings['reportWarning'];
		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['reportStrict'] = $this->prevLevelSettings['reportStrict'];

		unset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']);
	}

	/*
	 * This is an error handler function. To register it it must be public.
	 * The function is defined as a method so that we can remove it after a
	 * testcase run.
	 */
	public static function pedantic_errorHandlerCounter($errno, $errstr, $errfile, $errline) {
			switch ($errno) {
				case E_NOTICE:
					$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']['notices']++;
					break;
				case E_WARNING:
					$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']['warnings']++;
					break;
				case E_STRICT:
					$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['pedantic']['pedanticTestCaseSetUp']['strict']++;
					break;
				default:
					// Do nothing.
			}
	}

	public function testT3libDivHasNoStrictMessages() {
		// Create the Array fixture.
		$fixture = array();

		// Assert that the size of the Array fixture is 0.
		$this->assertEquals(0, sizeof($fixture));
	}

	public function testT3libDivHasNoWarnings() {
		// Create the Array fixture.
		$fixture = array();

		// Assert that the size of the Array fixture is 0.
		$this->assertEquals(0, sizeof($fixture));
	}

	public function testT3libDivHasNoNotices() {
		// Create the Array fixture.
		$fixture = array();

		// Assert that the size of the Array fixture is 0.
		$this->assertEquals(0, sizeof($fixture));
	}
}
?>